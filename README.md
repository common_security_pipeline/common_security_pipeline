## Назначение проекта:

построеине безопасности в цикле CI/CD 

## Технические детали
- pipeline предназначен для работы с Gitlab CI
- поддерживает и поставляется вместе со следующими тулами:
-- gitleaks
-- trufflehog
-- dependency check
-- gosec
-- phpcs
-- semgrep

## Что нужно что бы протестировать или запустить из  GitLab.com:
добавить в проект вашего .gitlab-ci.yml:
1. стейдж "security"
```
stages:
  - security
```
2. добавить джобу для стейджа:
```
call_common_security_pipeline:
  stage: security
  allow_failure: true
  trigger:
    include:
      - project: 'own_registry/common_security_pipeline'
        ref: 'master'
        file: 'pipeline/common_security_pipeline.yml'
```

## Чеклист на внедрение и доробку в своем контуре
1. Склонировать к себе common security pipeline
2. Склонировать к себе все репозитории security-тулами. Список тулов и откуда их клонить смотри в ./pipeline/security_tools.yml
3. Изменить в ./pipeline/security_tools.yml путь в registry до контейнеров в соотвествии с тем где вы их храните
4. Поднять у себя DefectDojo (см. https://github.com/DefectDojo/django-DefectDojo/blob/master/DOCKER.md)
5. Прописать у себя API-ключ, путь до DefectDojo для доступа к вашему DefectDojo в файле ./dd_prepare/dd_prepare.py. Если у вас используется Vault или аналогичное решение, то дороботать код проекта для получения API в runtime самостоятельно


### назначение ./dd_prepare 
1. creates a project, engagement in DD
2. define variables

### назначение ./pipeline
directory with main pipeline
1. Run docker with dd_prepare
2. Run security tools
3. upload results to DD

### назначение.gitlab-ci.yml
1. build
2. test



#TODO:
- генерация пайплайна в рантайме на основе шаблонов jinja2
- добавление security-тулов


## Замечания по безопасности
1. Секреты хранятся не в CI/CD variables т.к. запуск пайплайна происходит в режиме Parent-Child, при котором локально хранимые CI/CD variables полностью игнорируются. Хранить же ключи в репозиториях для проверки еще хуже. Решением является внедрение Hashicorp Vault, но интеграцию с ним в облачной версии GitLab не очень понятно, как показать
2. Некоторые образы security-тулов собираются с помощью выкачивания bash-скриптов. Если эта практика не соответствует требованиям вашей компании вы можете собрать контейнеры более безопасным способом
3. Semgrep запускается с настройками по умолчанию. Вопрос сбора данных при его работе не исследовался.












